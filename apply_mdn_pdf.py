#!/<specifyPath>/bin/python3
# @author:Ashim Rijal (a.rijal@uu.nl)
#
# import tensoflow and other libraries
import numpy as np
import os
import warnings
warnings.filterwarnings('ignore', category=FutureWarning)
from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False
#########################################################
#manage versions here
#import tensorflow as tf #for version 1
import tensorflow.compat.v1 as tf    #for version 2. even if version 2 is installed, I call version 1 because I worte most stuff on 1.
tf.disable_v2_behavior()        #disable version 2 behaviour
########################################################
import sys
import argparse
import pred_helper_functions
#
parser = argparse.ArgumentParser(description="Mixture Density Network prediction: posterior pdf (and optionally conditional average and standard deviation around that average) ")
parser.add_argument("test_fname", help="name of a data file (with absolute path) which contains test data")
parser.add_argument("num_mdns", nargs=1, type=int,help="total number of MDNs trained")
parser.add_argument("lo", nargs=1, type=float,help="lower bound of the ragne where each Gaussian kernel are to be evaluated")
parser.add_argument("up", nargs=1, type=float,help="upper bound of the range where each Gaussian kernel are to be evaluated")
parser.add_argument("nums", nargs=1, type=int, help="range [lo,up] will be evenly divided into nums points in total where each Gaussian kernel are to be evaluated")
parser.add_argument("import_dir_name", nargs=1, metavar=('path'), help="ABSOLUTE path to the directory where you saved trained MDN models (i.e path to MDN*_out directories)")
parser.add_argument("--out_fname", nargs=1, metavar=('string'),type=str, default=('pdf_.dat'), help="output file name")
args = parser.parse_args()
#
#
def mdn_apply(test_fname, import_dir_name, lo, up, nums, mdn_id, mdn_weight):
    # read the test data
    test_inputs = np.loadtxt(test_fname, dtype=np.float64, comments="#")
    #
    # what is your target (eg. volume) range of training data (go and see) before standardizing
    # but after modifying target type.
    cond_pdf_evl_pts = np.float64(np.linspace(lo, up, nums))
    #
    # access individual MDN directory
    each_mdn_directory = os.path.join(import_dir_name, "mdn%s_out/" % mdn_id)
    #
    # read useful information which was dumped during trainig each mdns
    nets_info_fname = "nets_info.dat"
    #
    with open(each_mdn_directory + nets_info_fname, "r") as f:
        x = f.read().splitlines()
    for word in x[1].split():
        input_dims = int(word)
    for word in x[3].split():
        target_dims = int(word)
    mean_and_std = []
    stds = []
    for word in x[5].split():
        mean_and_std.append(np.float64(word))
    for word in x[6].split():
        stds.append(np.float64(word))
    mus_i = np.expand_dims(np.array(mean_and_std), axis=0)
    sigs_i = np.expand_dims(np.array(stds), axis=0)
    mean_std_of_inputs = np.append(mus_i, sigs_i, axis=0)
    mean_and_std = []
    stds = []
    for word in x[8].split():
        mean_and_std.append(np.float64(word))
    for word in x[9].split():
        stds.append(np.float64(word))
    mus_t = np.expand_dims(np.array(mean_and_std), axis=0)
    sigs_t = np.expand_dims(np.array(stds), axis=0)
    mean_std_of_target = np.append(mus_t, sigs_t, axis=0)
    for word in x[11].split():
        KMIX = int(word)
    f.close()
    #
    # initialize session and restore the best trained model
    sess = tf.Session()
    signature_key = tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY
    input_key = 'x_input'
    output_key = 'y_output'
    meta_graph_def = tf.saved_model.loader.load(sess, [tf.saved_model.tag_constants.SERVING], each_mdn_directory)
    signature = meta_graph_def.signature_def
    x_tensor_name = signature[signature_key].inputs[input_key].name
    y_tensor_name = signature[signature_key].outputs[output_key].name
    x = sess.graph.get_tensor_by_name(x_tensor_name)
    output = sess.graph.get_tensor_by_name(y_tensor_name)
    # input dimension check
    if test_inputs.ndim == 1:
        if input_dims != 1:
            print("Dimension mismatch. Dimenstion of test data inputs must match traind data inputs")
        else:
            pass
    elif(input_dims != np.shape(test_inputs)[-1]):
        print("Dimension mismatch. Dimension mismatch. Dimenstion of test data inputs must match traind data inputs")
        sys.exit("STOP - Dimension mismatch")
    #
    # standardize test inputs
    if test_inputs.ndim == 1:
        test_inputs = test_inputs.reshape(test_inputs.size, 1)
        test_inputs = pred_helper_functions.standardize_test_data(test_inputs, mean_std_of_inputs[0, 0], mean_std_of_inputs[1, 0])
    else:
        for i in range(np.shape(test_inputs)[-1]):
            test_inputs[:, i] = pred_helper_functions.standardize_test_data(test_inputs[:, i], mean_std_of_inputs[0, i], mean_std_of_inputs[1, i])
    test_out = sess.run(output, feed_dict={x: test_inputs})
    #
    # unstandardize test inputs (outputs will be unstandardize later while evaluating pdf)
    if test_inputs.ndim == 1:
        test_inputs = pred_helper_functions.unstandardize(test_inputs, mean_std_of_inputs[0, 0], mean_std_of_inputs[1, 0])
    else:
        for i in range(np.shape(test_inputs)[-1]):
            test_inputs[:, i] = pred_helper_functions.unstandardize(test_inputs[:, i], mean_std_of_inputs[0, i], mean_std_of_inputs[1, i])
    test_out_pi, test_out_sigma, test_out_mu = pred_helper_functions.get_mixture_coef(test_out, KMIX)
    #
    test_out_pi = test_out_pi.eval(session=sess)
    test_out_sigma = test_out_sigma.eval(session=sess)
    test_out_mu = test_out_mu.eval(session=sess)
    #
    # considering we only have one target data, the following line is hard coded to normalize cond_pdf_evl_pts
    # here np.shape(test_inputs)[0] gives no. of x (eg. pressure) points where target pdf is being evaluated
    z = pred_helper_functions.evaluate_cond_pdf(np.shape(test_inputs)[0], pred_helper_functions.standardize_test_data(cond_pdf_evl_pts, mean_std_of_target[0, 0],mean_std_of_target[1, 0]), test_out_pi, test_out_mu, test_out_sigma)
    #
    # just divide by the std of the target data hard coded here
    z = z / mean_std_of_target[1, 0]
    #
    # uncomment following line to save pdf values for each MDNs (probably not needed in most of the cases)
    #np.savetxt(each_mdn_directory+"pdf_volume_"+test_fname, np.column_stack([test_inputs,z]),fmt="%20.10f", delimiter=' ')
    #
    sess.close()
    return(mdn_weight * z)
#
#
# program block
if __name__ == '__main__':
    print("Starting prediction... ")
    # read the test data
    test_fname = args.test_fname
    test_inputs = np.loadtxt(test_fname, dtype=np.float64, comments="#")
    #
    # what is your target (eg. volume) range of training data (go and see) before standardizing
    lo = args.lo[0]
    up = args.up[0]
    nums = args.nums[0]
    cond_pdf_evl_pts = np.float64(np.linspace(lo, up, nums))
    #
    # z_f to store final conditional pdf evaluation (i.e. weighted sum of pdfs from each network)
    z_f = np.zeros(((np.shape(test_inputs)[0], np.shape(cond_pdf_evl_pts)[0])), dtype=np.float64)
    #
    # collect ids and corresponding weights of all MDNs
    ids_weights = np.zeros(((args.num_mdns[0], 2)), dtype=np.float64)
    for k in range(args.num_mdns[0]):
        with open(os.path.join(args.import_dir_name[0], "mdn%s_out/" % k + "nets_info.dat"), "r") as f:
            x = f.read().splitlines()
        for word in x[17].split():
            mdn_id = int(word)
        for word in x[19].split():
            mdn_weight = np.float64(word)
        ids_weights[k, :] = mdn_id, mdn_weight
        #
    # all mdn_weights should sum to 1.0 to ensure that pdf integrates to 1.0
    ids_weights[:, 1] = ids_weights[:, 1] / (np.sum(ids_weights[:, 1]))
    #
    # mdn_apply function returns weighted pdf output, weighted by respective MDN weight.
    # sum all such weighted outputs to get final pdf.
    for k in range(args.num_mdns[0]):
        tf.reset_default_graph()
        z_f = z_f + mdn_apply(args.test_fname, args.import_dir_name[0], args.lo[0],args.up[0], args.nums[0], int(ids_weights[k, 0]), ids_weights[k, 1])
    #
    if not os.path.exists(os.path.join(args.import_dir_name[0], "average_mdn/")):
        os.mkdir(os.path.join(args.import_dir_name[0], "average_mdn/"))
    fname = open(os.path.join(args.import_dir_name[0], "average_mdn/", args.out_fname[0]), "w")
    np.savetxt(fname, np.column_stack([test_inputs, z_f]), fmt="%20.10f")
    fname.close()
    #
    print("")
    print("REPORT bug at: a.rijal@uu.nl or rijalashim@gmail.com")
    print("")
    print("----------------------------------------DONE!!!-----------------------------------------")
    print("")
    print("")
