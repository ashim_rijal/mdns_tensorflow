Mixture Density Networks of Bishop, 1994.

####
Ashim Rijal, Utrecht University, 2019.

If you have any queries, contact at: a.rijal@uu.nl or rijalashim@gmail.com

####
****Please cite the following paper if you use this code:
Rijal, A., Cobden, L., Trampert, J. (2023). SeisTeC: A neural network tool to constrain mantle thermal
and chemical properties from seismic observables, submitted to PEPI.****
####


Implementation of Mixture Density Network (MDN) of Bishop (1994):

train_deep_mdn.py ---> construct and train an MDN

apply_mdn_pdf.py ---> predict posterior probability density functions (pdf) using a trained MDN

####


**** It is very important to understand that this MDN implementation is only for 1D target data ***

**** train_deep_mdn.py ****

Basic usage:
    python train_deep_mdn.py <train_data_fname> <test_data_fname> [options]

This constructs and trains an MDN using mandatory  <train_data_fname> and <test_data_fname> files.
    python train_deep_mdn.py --help prints out help messages.

    [optins] are:
    --monitor_size=yy     yy is a float which specifies the size of monitor data, default is 0.22 or 22% of training data

    --mdn_id=o            where o is of type integer which specifies the identity number of the Mixture Density Network that is being trained. Default is 0. (see slurm job submission script to train multiple MDNs in parallel)

    --num_hidden_layers=p where p is an integer which specifies the number of hidden layers in the MDN

    --lo_num_hid_nodes=q  where q is 1D list of type integer which specifies the lower bounds for number of hidden nodes in each layer. If not supplied, default lower bounds will be used.

    --up_num_hid_nodes=r  where r is 1D list of type integer which specifies the upper bounds for number of hidden nodes in each layer. If not supplied, default upper bounds will be used.

    --lo_num_kernels=s    where s is is of type integer which specifies the lower bound for the number of Gaussian kernels to use. If not supplied, default lower bound for Gaussian kernels will be used.

    --up_num_kernels=t    where t>=s is of type integer which specifies the upper bound for the number of Gaussian kernels to use. If not supplied, default lower bound for Gaussian kernels will be used.

    --num_epoch=u         where u is of type integer which specifies the number of epochs to perform during training. If not supplied, default value will be used.
    --mini_batch_size=v   where v is of type integer which specifies the mini batch size; 0 means use whole batch.

####


**** apply_mdn_pdf.py ****

Basic usage:
    python apply_mdn_pdf.py <prediction_fname> <num_mdns> <lo> <up> <nums> <model_dir> [options]
    
This script uses a trained MDN to make predictions (posterior pdf) for a given input in <prediction_fname> file (see below for data format).
    python apply_mdn_pdf.py --help prints out help messages.

    Along with <prediction_fname> file, mandatory arguments are <num_mdns> <lo> <up> <nums> and <model_dir>

    num_mdns               total number of MDNs trained during training.

    model_dir              absolute path to the directory where you saved trained MDN models (i.e path to MDN*_out directories)

    lo                     a real number which specifies the lower bound for posterior pdf evaluation.

    up                     a real number which specifies the upper bound for posterior pdf evaluation.

    nums                   an integer which specifies the number of posterior pdf evaluation points. These points will be evenly distributed between <lo> and <up>.

    [optins] are:

    --out_fname=s          s is a string which specifies the name you want to attach to output files.

####


**** data format ****

1) <train_data_fname> and <test_data_fname> should be in following format

    inp_11 inp_12 ..... inp_1n inp_1_tar

    inp_21 inp_22 ..... inp_2n inp_2_tar

    ...... ...... ..... ...... .........

    inp_k1 inp_k2 ..... inp_kn inp_k_tar

where inp_\* are inputs, and inp_\*_tar are target data. n is number of input dimension and k in number of patterns.

Here we only deal with 1D target data and it should be the last column in the data file.

PLESE NOTE: while using mdn_apply_pdf.py, the data format of the <prediction_fname> is the same 1) above but shouldn't contain target data (i.e. the last column).

2) you can have the header of the file but must be commented with "#" such that the commented lines are omitted while reading data.

3) predicted posterior pdf output file has following format

    inp_11 inp_12 ..... inp_1n pdf_11 pdf_12 ..... pdf_1m
    
    inp_21 inp_22 ..... inp_2n pdf_21 pdf_22 ..... pdf_2m
    
    ...    ...   .....  ...    ...    ...   .....  ...
    
    inp_k1 inp_k2 ..... inp_kn pdf_k1 pdf_k2 ..... pdf_km

where inp_\* are inputs, pdf_\* are values of posterior pdf evaluated at \* point. n is number of input dimension and k is number of input patterns.

m denotes the number of points where posterior pdf is evaluated (which is basically <nums> argument supplied with apply_mdn_pdf.py script).

****Please note: inputs are copied to output files
####


****     How to run the training script ****

1) train only one MDN directly from command line:

python train_deep_mdn.py train_data.dat test_data.dat --monitor_size 0.22 --mdn_id 0 --num_hidden_layers 1 --lo_num_hid_nodes 10 --up_num_hid_nodes 20 --lo_num_kernels 3 --up_num_kernels 5 --num_epoch 15000 --mini_batch_size=512

where, train_data.dat and test_data.dat are mandatory input data files. Other arguments are optional, if not provided then default values will be used. All output will be dumped into a directory named mdn0_out/ . Please note "0" in the name of the directory comes from --mdn_id=0 argument.


2) train multiple MDNs in parallel:

You can train multiple MDNs in parallel. This can be accomplised using an example (see submit_train.sbatch) slurm job submission script. Please note you need GNU Parallel in this example.


****     How to predict posterior pdf using trained nets ****

1) directly from commmand line:

python apply_mdn_pdf.py prediction_data.dat 10 40 80 400 <path> --out_fname=predicted_pdf.dat

2) using slurm job submission:

This can be accomplised using an example (see submit_apply_pdf.sbatch) slurm job submission script.


**** References ****

Bishop, C.M., 1994. Mixture Density Networks, Neural Computing Research Group Report: NCRG/94/004, Aston University.

David Ha, 2015. Mixture Density Networks with TensorFlow.

Abadi et al., 2015. TensorFlow: Large scale machine learning on heterogeneous systems.
####

