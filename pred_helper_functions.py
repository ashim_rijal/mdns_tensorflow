#!/<specifyPath>/bin/python3
# @author:Ashim Rijal (a.rijal@uu.nl)
# Some code snippets are taken from different sources. References (also for details of MDNs) are given below:
# Bishop, C. M. (1994). Mixture density networks.
# David Ha, 2015. Mixture Density Networks with TensorFlow
#
# import tensoflow and other libraries
import numpy as np
#########################################################
#manage versions here
#import tensorflow as tf #for version 1
import tensorflow.compat.v1 as tf    #for version 2. even if version 2 is installed, I call version 1 because I worte most stuff on 1.
tf.disable_v2_behavior()        #disable version 2 behaviour
########################################################
import os
import sys
import matplotlib as mpl
#
# All functions
# standardize test data
def standardize_test_data(X, mean, std):
    z = ((X - mean) / std)
    return z
#
# unstandardize data
def unstandardize(z, stand_mean, stand_sd):
    X = (z * stand_sd) + stand_mean
    return X
#
def get_mixture_coef(output, KMIX):
    out_pi = tf.placeholder(dtype=tf.float64, shape=[None, KMIX], name="mix_para_weights")
    out_sigma = tf.placeholder(dtype=tf.float64, shape=[None, KMIX], name="mix_para_sigma")
    out_mu = tf.placeholder(dtype=tf.float64, shape=[None, KMIX], name="mix_para_mu")
    out_pi, out_sigma, out_mu = tf.split(output, 3, 1)
    max_pi = tf.reduce_max(out_pi, 1, keep_dims=True)
    out_pi = tf.subtract(out_pi, max_pi)
    out_pi = tf.exp(out_pi)
    normalize_pi = tf.reciprocal(tf.reduce_sum(out_pi, 1, keep_dims=True))
    out_pi = tf.multiply(normalize_pi, out_pi)
    out_sigma = tf.exp(out_sigma)
    #
    return out_pi, out_sigma, out_mu
#
# some functions to evaluate conditional pdfs (i.e. MDN output)
def evaluate_cond_pdf(num_eval_pts, b, weights, mus, sigmas):
    z = np.zeros(((num_eval_pts, len(b))), dtype=np.float64)
    for i in range(num_eval_pts):
        z[i, :] = evaluate_gaussian_mixtures(b, weights[i, :], mus[i, :], sigmas[i, :])
    return z
#
def evaluate_gaussian_mixtures(b, weights, mus, sigmas):
    p = np.zeros((len(b)), dtype=np.float64)
    for i in range(len(weights)):
        p = p + weights[i] * custom_gaussian(b, mus[i], sigmas[i])
    return p
#
def custom_gaussian(b, mu, std):
    result = np.exp((-(b - mu)**2.0) / (2.0 * std**2.0)) / (std * (2.0 * np.pi)**0.5)
    return result
#
# conventional least square as a special case of MDNs
def mean_from_mdn_as_lsq(num_eval_pts, weights, mus):
    conditional_average = np.zeros(((num_eval_pts)), dtype=np.float64)
    for i in range(num_eval_pts):
        conditional_average[i] = evaluate_means(weights[i, :], mus[i, :])
    return conditional_average
#
def evaluate_means(weights, mus):
    mean = 0.0
    for j in range(len(weights)):
        mean = mean + (weights[j] * mus[j])
    return mean
#
# variance around the conditional mean
def variance_around_cond_mean(num_eval_pts, weights, mus, sigmas):
    conditional_variance = np.zeros(((num_eval_pts)), dtype=np.float64)
    for i in range(num_eval_pts):
        conditional_variance[i] = evaluate_variance(weights[i, :], mus[i, :], sigmas[i, :])
    return conditional_variance
#
def evaluate_variance(weights, mus, sigmas):
    variance = 0.0
    for j in range(len(weights)):
        variance = variance + (weights[j] * (sigmas[j]**2 + (mus[j] - evaluate_means(weights, mus))**2))
    return variance
#
