import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.rc('font', family='Times')
matplotlib.rcParams.update({'font.size': 27})
#
#
def read_weights(import_dir_name, num_mdns):
    ids_weights= np.zeros(((num_mdns,2)), dtype=np.float64)
    for k in range(num_mdns):
        with open(import_dir_name+"mdn%s_out/"%k+"nets_info.dat", "r") as f:
            x = f.read().splitlines()
        for word in x[19].split():
            mdn_id = int(word)
        for word in x[21].split():
            mdn_weight = np.float64(word)
        ids_weights[k, :] = mdn_id, mdn_weight
    # all mdn_weights should sum to 1.0 to ensure that pdf integrates to 1.0
    #print(ids_weights)
    ids_weights[:,1]= ids_weights[:,1]/(np.sum(ids_weights[:,1]))
    return ids_weights
#
nodes= np.arange(10,120,10)
train_error=[]
val_error=[]
test_error=[]
num_mdns= 20
test_data_size= 1000.
for i in nodes:
    import_dir_name= "<specifyPath>/hid_%s/"%i
    #read weight of each MDN
    ids_weights= read_weights(import_dir_name, num_mdns)
    _temp_train=[]
    _temp_val=[]
    _temp_test=[]
    error= np.zeros(3)
    for j in range(num_mdns):
        fname="/specifyPath/mdn%s_out/iter_vs_errors.dat"%(i,j)
        temp_error= np.loadtxt(fname, skiprows=1, dtype=np.float64)
        ######################
        error= error+(ids_weights[j,1]*(temp_error[np.argmin(temp_error[temp_error[:,2]!=0][:,2])]))
        #
    train_error.append(error[0])
    val_error.append(error[1])
    test_error.append(error[2])
#
plt.figure(figsize=(14, 8))
plt.plot(nodes, np.array(val_error), c='r',ls='--', label='Error-val')
plt.plot(nodes, np.array(test_error), c='b', ls='--', label='Error-test')
#
plt.xlabel("hiddne nodes")
plt.ylabel("Error")
legend = plt.legend(fontsize=25, fancybox=True, framealpha=1, loc=1)
legend.get_frame().set_facecolor('white')
plt.savefig("./sample.png")
plt.close()
