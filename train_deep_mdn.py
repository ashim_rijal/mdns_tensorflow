#!/<specifyPath>/bin/python3
# @author:Ashim Rijal (a.rijal@uu.nl)
#
# import libraries
import argparse
import numpy as np
import train_helper_functions
#
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="deep Mixture Density Network training")
    parser.add_argument("train_data_fname", help="name of a data file (with ABSOLUTE path) which contains training data")
    parser.add_argument("test_data_fname", help="name of a data file (with ABSOLUTE path) which contains testing data")
    parser.add_argument("--monitor_size", nargs=1, metavar=('float'), type=float, default=('0.22'), help="what fraction of data is to be used as monitor/validation data; default is 0.22")
    parser.add_argument("--mdn_id", nargs=1, metavar=('integer'), type=int,default=('0'), help="id of the mixture density network; default is 0")
    parser.add_argument("--num_hidden_layers", nargs=1, metavar=('integer'), type=int, default=('1'), help="number of hidden layers; default is 1hiddden layer network")
    parser.add_argument("--lo_num_hid_nodes", nargs='+', metavar=('integers'), type=int, default=('15'), help="a 1D list containing lower bounds for the number of hidden nodes in each layer; default is 15")
    parser.add_argument("--up_num_hid_nodes", nargs='+', metavar=('integers'), type=int, default=('35'), help="a 1D list containing upper bounds for the number of hidden nodes in each layer; default is 35")
    parser.add_argument("--lo_num_kernels", nargs=1, metavar=('integer'), type=int,default=('3'), help="lower bound for the number of Gaussian kernels; default is 3")
    parser.add_argument("--up_num_kernels", nargs=1, metavar=('integer'), type=int,default=('5'), help="upper bound for the number of Gaussian kernels; default is 5")
    parser.add_argument("--num_epoch", nargs=1, metavar=('integer'), type=int,default=('1000'), help="number of epochs; default is 1000")
    parser.add_argument("--mini_batch_size", nargs=1, metavar=('float'), type=float,
                        default=('0'), help="mini-batch training size; default is 0 i.e. use whole batch")
    args = parser.parse_args()
    #
    #
    print("")
    print("****************************************************************************************")
    print("-----------------------@@------@@-------@@@@@----------@@@-----@@@----------------------")
    print("-----------------------@@@----@@@-------@@@@@@@--------@@@@----@@@----------------------")
    print("-----------------------@@@@--@@@@-------@@@--@@@-------@@@@@---@@@----------------------")
    print("-----------------------@@@@@@@@@@-------@@@--@@@-------@@@-@@--@@@----------------------")
    print("-----------------------@@@@@@@@@@-------@@@--@@@-------@@@--@@-@@@----------------------")
    print("-----------------------@@@----@@@-------@@@--@@@-------@@@---@@@@@----------------------")
    print("-----------------------@@@----@@@-------@@@@@@@--------@@@----@@@@----------------------")
    print("-----------------------@@@----@@@-------@@@@@----------@@@-----@@@----------------------")
    print("----------------------------------------------------------------------------------------")
    print("----------------------------------------------------------------------------------------")
    print("")
    print("*************** to report bug use a.rijal@uu.nl or rijalashim@gmail.com ****************")
    print("----------------------------------------------------------------------------------------")
    #
    # do some initial checks and call MDN build and train function
    if np.array(args.lo_num_hid_nodes).ndim == 1:
        if np.array(args.up_num_hid_nodes).ndim == 1:
            if len(np.array(args.lo_num_hid_nodes))== args.num_hidden_layers[0]:
                if len(np.array(args.up_num_hid_nodes))== args.num_hidden_layers[0]:
                    # call MDN building and training function
                    train_helper_functions.train_deep_mdn(args.train_data_fname, args.test_data_fname, args.monitor_size[0], args.num_hidden_layers[0], np.array(args.lo_num_hid_nodes), np.array(args.up_num_hid_nodes), args.lo_num_kernels[0], args.up_num_kernels[0], args.num_epoch[0], args.mdn_id[0], args.mini_batch_size[0])
                else:
                    print("Error, length of --up_num_hid_nodes doesn't match the number of hidden layer")
            else:
                print("Error, length of --lo_num_hid_nodes doesn't match the number of hidden layer")
        else:
            print("Error, dimension of list --up_num_hid_nodes must be 1")
    else:
        print("Error, dimension of list --lo_num_hid_nodes must be 1")
##########################################################
