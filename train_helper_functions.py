#!/<specifyPath>/bin/python3
# @author:Ashim Rijal (a.rijal@uu.nl)
# Some code snippets are taken from different sources. References (also for details of MDNs) are given below:
# Bishop, C. M. (1994). Mixture density networks.
# David Ha, 2015. Mixture Density Networks with TensorFlow
#
#
# import tensoflow and other libraries
import matplotlib
# on MAC OSX sometimes direct import of pyplot throws error.
matplotlib.use('PS')
import matplotlib.pyplot as plt
import numpy as np
import warnings
warnings.filterwarnings('ignore', category=FutureWarning)
from tensorflow.python.util import deprecation
deprecation._PRINT_DEPRECATION_WARNINGS = False
#########################################################
#manage versions here
#import tensorflow as tf #for version 1
import tensorflow.compat.v1 as tf    #for version 2. even if version 2 is installed, I call version 1 because I worte most stuff on 1.
tf.disable_v2_behavior()        #disable version 2 behaviour
########################################################
import math
import random
from datetime import datetime
from sklearn.model_selection import train_test_split
import os
import sys
import pandas as pd
import matplotlib as mpl
from sklearn.cluster import KMeans
import collections
from scipy.stats import multivariate_normal
import time
###############################################
#changes to activate GPUs
# Enable '0' or disable '-1' GPU use
# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = "0"
###############################################
#
# All functions
# standardize data
def standardize(X):
    z = ((X - np.mean(X)) / np.std(X))
    stand_mean = np.mean(X)
    stand_sd = np.std(X)
    return z, stand_mean, stand_sd

# standardize test data


def standardize_test_data(X, mean, std):
    z = ((X - mean) / std)
    return z

# unstandardize data


def unstandardize(z, stand_mean, stand_sd):
    X = (z * stand_sd) + stand_mean
    return X
# NOTE: this is only 1D version of Kmeans clustering. i.e. it works for 1D output
# TODO: implement for multi dimensional output
def oneD_within_sum_of_squares(cluster_nums, data, centroids, labels):
    cluster_energy = np.zeros(((cluster_nums)), dtype=np.float32)
    num_points = np.shape(data)[0]
    for i in range(num_points):
        j = labels[i]
        cluster_energy[j] = cluster_energy[j] + np.sum((data[i] - centroids[j])**2)
    return cluster_energy
#
def initialize_biases(y_train, KMIX):
    cluster_nums = KMIX
    kmeans = KMeans(n_clusters=cluster_nums, max_iter=10000).fit(y_train)
    clusters = kmeans.cluster_centers_
    energy = oneD_within_sum_of_squares(cluster_nums, y_train, clusters, kmeans.labels_)
    population = np.array(list((collections.Counter(kmeans.labels_)).items()), dtype=np.float32)
    pi = np.log(population[:, -1])
    std = np.log(np.sqrt(energy / population[:, -1]))
    mu = clusters.reshape(clusters.size)
    return np.concatenate((pi, std, mu), axis=0)
#
# modified ELU
def modify_elu(x, a=1.0):
    e=1.0e-15
    return tf.nn.elu(x)+1.0+e
#
# define a function that outputs mean, standard deviation and wegits of each Gaussian kernel
def get_mixture_coef(output, KMIX):
    out_pi = tf.placeholder(dtype=tf.float32, shape=[None, KMIX], name="mix_para_weights")
    out_sigma = tf.placeholder(dtype=tf.float32, shape=[None, KMIX], name="mix_para_sigma")
    out_mu = tf.placeholder(dtype=tf.float32, shape=[None, KMIX], name="mix_para_mu")
    # split the output into weight, standard deviation and mean
    out_pi, out_sigma, out_mu = tf.split(output, 3, 1)
    # sometimes it is helpful to clip out_pi because applying softmax to small weights (i.e. Gaussian kernals with small weight--> less significant) 
    # can lead to even smaller numerical values
    # out_pi = tf.clip_by_value(out_pi, 1.0e-5, 1.0)
    max_pi = tf.reduce_max(out_pi, 1, keep_dims=True)
    out_pi = tf.subtract(out_pi, max_pi)
    out_pi = tf.exp(out_pi)
    normalize_pi = tf.reciprocal(tf.reduce_sum(out_pi, 1, keep_dims=True))
    out_pi = tf.multiply(normalize_pi, out_pi)
    # out_sigma = tf.exp(out_sigma)
    out_sigma = modify_elu(out_sigma)
    #
    return out_pi, out_sigma, out_mu
#
# minimize the negative logarithm of the likelihood (see equation 28 and 29 of Bishop, 1994)
# loss function
# evaluation of exponential term of equation 29
# def tf_normal(y, mu, sigma, norm_factor):
#     result = tf.subtract(y, mu)
#     result = tf.multiply(result, tf.reciprocal(sigma))
#     result = -tf.square(result) / 2
#     return tf.multiply(tf.exp(result), tf.reciprocal(sigma)) * norm_factor
#
# log_sum_exp trick(?)
def log_sum_exp(result):
    max_result = tf.reduce_max(result, 1, keep_dims=True)
    return tf.log(tf.reduce_sum(tf.exp(result - max_result), axis=1, keep_dims=True)) + max_result
#
# compute loss
def get_lossfunc(output, y, KMIX):
    # normalization factor see eq.23 of Bishop's MDN (Bishop, C., 1994)
    #norm_factor = 1.0 / math.sqrt(2.0 * math.pi)
    out_pi, out_sigma, out_mu = get_mixture_coef(output, KMIX)
    # for the log-sum-exp-trick
    exponent = tf.log(out_pi) - 0.5*np.log(2.*np.pi) - tf.log(out_sigma) -\
    0.5*tf.square(tf.multiply(tf.subtract(y, out_mu), tf.reciprocal(out_sigma)))
    result = -1.0*log_sum_exp(exponent)
    # or can do calculations this way as well.
    #result = tf_normal(y, out_mu, out_sigma, norm_factor)
    #result = tf.multiply(result, out_pi)
    #result = tf.reduce_sum(result, 1, keep_dims=True)
    #result = -tf.log(result)
    return tf.reduce_mean(result)
#
# some functions to evaluate conditional pdfs (i.e. MDN output)
def evaluate_cond_pdf(num_eval_pts, b, weights, mus, sigmas):
    z = np.zeros(((num_eval_pts, len(b))), dtype=np.float32)
    for i in range(num_eval_pts):
        z[i, :] = evaluate_gaussian_mixtures(
            b, weights[i, :], mus[i, :], sigmas[i, :])
    return z
#
def evaluate_gaussian_mixtures(b, weights, mus, sigmas):
    p = np.zeros((len(b)), dtype=np.float32)
    for i in range(len(weights)):
        p = p + weights[i] * custom_gaussian(b, mus[i], sigmas[i])
    return p
#
def custom_gaussian(b, mu, std):
    result = np.exp((-(b - mu)**2.0) / (2.0 * std**2.0)) / \
        (std * (2.0 * np.pi)**0.5)
    return result
#
# construct hidden layer
def construct_hid_layer(layer, in_size, out_size, layer_number):
    wh= tf.Variable(tf.random_normal([in_size, out_size], stddev=(1.0 / (1.0 + in_size)), dtype=tf.float32), name="wh%s"%layer_number)
    bh= tf.Variable(tf.random_normal([1, out_size], stddev=(1.0 / (1.0 + in_size)), dtype=tf.float32), name="bh%s"%layer_number)
    #return tf.nn.leaky_relu(tf.matmul(layer, wh) + bh, alpha=0.2, name="hid_layer%s"%layer_number)
    return tf.nn.sigmoid(tf.matmul(layer, wh) + bh, name="hid_layer%s"%layer_number)
#
# for mini-batch
# Randomizes the order of data samples and their corresponding labels
def randomize(x, y):
    permutation = np.random.permutation(y.shape[0])
    shuffled_x = x[permutation, :]
    shuffled_y = y[permutation]
    return shuffled_x, shuffled_y
#
def get_next_batch(x, y, start, end):
    x_batch = x[start:end]
    y_batch = y[start:end]
    return x_batch, y_batch
#
#
# train MDNs
def train_deep_mdn(train_data_fname, test_data_fname, monitor_size, num_hidden_layers, lo_num_hid_nodes, up_num_hid_nodes, lo_num_kernels, up_num_kernels, num_epoch, k, mini_batch_size):
    print("                             training MDN%s started!!! " % k)
    print("****************************************************************************************")
    print("")
    print("")
    print("")
    # track time required to do train a network
    start_time = time.time()
    #
    ######
    initial_data = np.loadtxt(train_data_fname, dtype=np.float32, comments="#")
    train_data, val_data = train_test_split(initial_data, shuffle=True, test_size=monitor_size)
    test_data = np.loadtxt(test_data_fname, dtype=np.float32, comments="#")
    ######
    np.random.shuffle(train_data)
    np.random.shuffle(test_data)
    np.random.shuffle(val_data)
    #
    # create a directory (if doesn't exit) where we dump all necessary network information
    # and also dump the best trained MDN
    #
    if not os.path.exists(os.path.join(os.path.abspath(os.getcwd()), "mdn%s_out/" % k)):
        os.mkdir(os.path.join(os.path.abspath(os.getcwd()), "mdn%s_out/" % k))
    save_directory = os.path.join(os.path.abspath(os.getcwd()), "mdn%s_out/" % k)
    #
    print("train_data_shape= ", np.shape(train_data))
    print("val_data_shape= ", np.shape(val_data))
    print("test_data_shape= ", np.shape(test_data))
    #
    # extract inputs and targets
    train_input_data = train_data[:, :-1]         # all columns except last
    train_target_data = train_data[:, -1]         # just the last column
    #
    val_input_data = val_data[:, :-1]         # all columns except last
    val_target_data = val_data[:, -1]         # just the last column
    #
    test_input_data = test_data[:, :-1]         # all columns except last
    test_target_data = test_data[:, -1]         # just the last column
    #
    # x_* is input tensor and y_* is output/target
    if train_input_data.ndim == 1:
        x_train = train_input_data.reshape(train_input_data.size, 1)
    else:
        x_train = train_input_data
    # 
    y_train = train_target_data.reshape(train_target_data.size, 1)
    #
    if val_input_data.ndim == 1:
        x_val = val_input_data.reshape(val_input_data.size, 1)
    else:
        x_val = val_input_data
    y_val = val_target_data.reshape(val_target_data.size, 1)
    #
    if test_input_data.ndim == 1:
        x_test = test_input_data.reshape(test_input_data.size, 1)
    else:
        x_test = test_input_data
    y_test = test_target_data.reshape(test_target_data.size, 1)
    #
    # normalize inputs and targets
    # TODO: write a function to do this-->it looks messy at the moment.
    # store means and standard deviations of inputs and targets
    mean_std_of_inputs = np.zeros(((2, np.shape(x_train)[-1])), dtype=np.float32)
    mean_std_of_target = np.zeros(((2, np.shape(y_train)[-1])), dtype=np.float32)
    for i in range(np.shape(x_train)[-1]):
        x_train[:, i], mean_std_of_inputs[0,i], mean_std_of_inputs[1, i] = standardize(x_train[:, i])
    for j in range(np.shape(y_train)[-1]):
        y_train[:, j], mean_std_of_target[0,j], mean_std_of_target[1, j] = standardize(y_train[:, j])
    for i in range(np.shape(x_val)[-1]):
        x_val[:, i] = standardize_test_data(x_val[:, i], mean_std_of_inputs[0, i], mean_std_of_inputs[1, i])
    for j in range(np.shape(y_val)[-1]):
        y_val[:, j] = standardize_test_data(y_val[:, j], mean_std_of_target[0, j], mean_std_of_target[1, j])
    for i in range(np.shape(x_test)[-1]):
        x_test[:, i] = standardize_test_data(x_test[:, i], mean_std_of_inputs[0, i], mean_std_of_inputs[1, i])
    for j in range(np.shape(y_test)[-1]):
        y_test[:, j] = standardize_test_data(y_test[:, j], mean_std_of_target[0, j], mean_std_of_target[1, j])
    #
    # construct a network
    # first define some parameters, KMIX= number of kernels in the GMM
    KMIX = random.randint(lo_num_kernels, up_num_kernels)
    NOUT = KMIX * 3
    #
    # initialze output layer's bias using k-means clustering.
    out_bias = initialize_biases(y_train, KMIX)
    out_bias = out_bias.reshape(1, out_bias.size)
    #
    # define placeholders
    x = tf.placeholder(dtype=tf.float32, shape=[None, np.shape(x_train)[-1]], name="x")
    y = tf.placeholder(dtype=tf.float32, shape=[None, 1], name="y")
    #
    # hidden layers and their weights and biases.
    # size of the input vector (or the input dimension)
    input_size = np.shape(x_train)[-1]
    hidden_= x
    #
    num_nodes= np.zeros(num_hidden_layers, dtype=np.int32)
    for layer_num in range(num_hidden_layers):
        num_nodes[layer_num]= random.randint(lo_num_hid_nodes[layer_num], up_num_hid_nodes[layer_num])
    #
    for i in range(num_hidden_layers):
        hidden_= construct_hid_layer(hidden_, input_size, num_nodes[i], i+1)
        input_size= num_nodes[i]
    #
    # weights and biases of output layer
    wo = tf.Variable(tf.random_normal([input_size, NOUT], stddev=(1.0 / (1.0 + input_size)), dtype=tf.float32), name="wo")
    #bo = tf.Variable(tf.random_normal([1,NOUT], stddev=0.5, dtype=tf.float32),name="bo")
    bo = tf.get_variable(name="bo", dtype=tf.float32, initializer=out_bias)
    #
    # define output layer
    output = tf.matmul(hidden_, wo) + bo
    #
    # evaluate equation 29 (but for all patterns)
    lossfunc = get_lossfunc(output, y, KMIX)
    #
    # set the desired optimizer
    train_op = tf.train.AdamOptimizer().minimize(lossfunc)
    #
    # clip gradients (e.g., in case of NaN losses)
    # clip_norm = 1.0 # choose this value according to your problem.
    # grads = tf.gradients(lossfunc, tf.trainable_variables())
    # grads, _ = tf.clip_by_global_norm(grads, clip_norm)  # gradient clipping
    # grads_and_vars = list(zip(grads, tf.trainable_variables()))
    # optimizer = tf.train.AdamOptimizer()
    # train_op = optimizer.apply_gradients(grads_and_vars)
    #
    #######################################################
    #manage version here
    #sess = tf.InteractiveSession()                            #without gpu and with v1
    #######################################################
    #changes to activate GPUs of v2
    config = tf.ConfigProto()
    # config.gpu_options.visible_device_list = "0"
    config.gpu_options.allow_growth = True
    #config.gpu_options.per_process_gpu_memory_fraction = 0.034 #set specific memory (eg. 0.03325)
    ######################################################
    init= tf.global_variables_initializer()
    #
    # Stop optimization if no improvement found in this many epochs.
    require_improvement = 100
    #
    # set some global variables and mini-batch
    if mini_batch_size==0:
        batch_size = len(y_train)/1.
    elif mini_batch_size>len(y_train):
        print("Mini batch size is larger than total number of training examples! Setting mini batch to whole batch!")
        batch_size = len(y_train)/1.
    else:
         batch_size = mini_batch_size
    print("batch size= ", batch_size)
    num_tr_iter = int(len(y_train) / batch_size)
    # Counter for total number of training epoches performed so far.
    total_epochs = 0
    # Epoch-number for last improvement to validation/monitoring accuracy.
    last_improvement = 0
    # Best validation/monitoring accuracy seen so far.
    best_validation_error = 1.0e+50
    best_test_error = 0.0
    train_error = np.zeros((num_epoch), dtype=np.float32)
    val_error = np.zeros((num_epoch), dtype=np.float32)
    test_error = np.zeros((num_epoch), dtype=np.float32)
    #
    # define a session object and start training
    with tf.Session(config=config) as sess:
        sess.run(init)
        ########sess.graph.finalize() #finalize graph to see if memory is leaking
        for i in range(num_epoch):
            total_epochs += 1
            x_train, y_train = randomize(x_train, y_train)
            # mini-batch training
            for j in range(num_tr_iter):
                start = j * int(batch_size)
                end = (j + 1) * int(batch_size)
                x_batch, y_batch = get_next_batch(x_train, y_train, start, end)
                sess.run(train_op, feed_dict={x: x_batch, y: y_batch})
            # compute train, test, monitor error after each epoch and store
            train_error[i] = sess.run(lossfunc, feed_dict={x: x_train, y: y_train})
            val_error[i] = sess.run(lossfunc, feed_dict={x: x_val, y: y_val})
            test_error[i] = sess.run(lossfunc, feed_dict={x: x_test, y: y_test})
            current_val_error = val_error[i]
            current_test_error = test_error[i]
            #
            # Print status every xx epochs or after last epoch.
            if (total_epochs % 10 == 0) or (i == (num_epoch - 1)):
                if (current_val_error < best_validation_error):
                    best_validation_error = current_val_error
                    best_test_error = current_test_error
                    # Set the epoch for the last improvement to current.
                    last_improvement = total_epochs
                    #
                    # save model: Saving model at each iteration with this approach consumes a lot of memory
                    # This is because we create builder every iteration, and each builder remains in the graph (and hence memory) 
                    # For a big NN model or for a NN with large training examples find a way to remove old builders from the graph and only save the last best one.
                    # save the tensorflow model. later we will restore it in mdn_apply script
                    builder = tf.saved_model.builder.SavedModelBuilder(save_directory)
                    tensor_info_x = tf.saved_model.utils.build_tensor_info(x)
                    tensor_info_y = tf.saved_model.utils.build_tensor_info(output)
                    prediction_signature = (tf.saved_model.signature_def_utils.build_signature_def(inputs={'x_input':tensor_info_x}, outputs={'y_output': tensor_info_y},method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))
                    builder.add_meta_graph_and_variables(sess, [tf.saved_model.tag_constants.SERVING], signature_def_map={tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY:prediction_signature},)
                    builder.save()
                    #
                    # string to indicate if the training is improved
                    improved_str = 'improved'
                else:
                    # string to be printed below, shows that no improvement was found.
                    improved_str = 'not-improved'
                    #
            # If no improvement found in the required number of epochs.
            if total_epochs - last_improvement > require_improvement:
                break
    # dump error information into a file
    ferror = open(save_directory + "iter_vs_errors.dat", "w+")
    ferror.write("train_error          monitor_error            test_error")
    ferror.write("\n")
    np.savetxt(ferror, np.column_stack([train_error, val_error, test_error]), fmt="%20.10f")
    ferror.close()
    #
    # dump useful information which we will require during prediction
    #TODO: find a better way to dump these information!
    f = open(save_directory + "nets_info.dat", "w+")
    f.write("#input_dims")
    f.write("\n")
    f.write(str(np.shape(x_train)[-1]))
    f.write("\n")
    f.write("#target_dims")
    f.write("\n")
    f.write(str(np.shape(y_train)[-1]))
    f.write("\n")
    f.write("#mean_and_std_of_inputs")
    f.write("\n")
    np.savetxt(f, mean_std_of_inputs, fmt="%20.10f")
    f.write("#mean_and_std_of_targets")
    f.write("\n")
    np.savetxt(f, mean_std_of_target, fmt="%20.10f")
    f.write("#number_of_Gaussian_kernels")
    f.write("\n")
    f.write(str(KMIX))
    f.write("\n")
    f.write("#total_epochs_performed")
    f.write("\n")
    f.write(str(total_epochs))
    f.write("\n")
    f.write("#number of hidden nodes in each hidden layer")
    f.write("\n")
    f.write(str(num_nodes))
    f.write("\n")
    f.write("#mdn_id")
    f.write("\n")
    f.write(str(k))
    f.write("\n")
    f.write("#mdn_weight")
    f.write("\n")
    f.write(str(np.exp(-best_test_error/np.shape(y_test)[0])))
    f.write("\n")
    f.write("#epoch_number_at_which_best_model_is_saved, i.e. epoch of last improvement")
    f.write("\n")
    f.write(str(last_improvement))
    f.write("\n")
    f.close()
    #
    print("")
    print("total time taken= ", time.time() - start_time, "s to train this MDN")
    print("******************** training MDN%s done!!! ********************" % k)
    print("")
    print("")
#
#
